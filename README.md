## stb_cmake
CMake helper for stb (https://github.com/nothings/stb). 

#### Purpose
stb_cmake does four things: 

1. Adds stb as a cmake target
2. Allows stb to be compiled into a library, avoiding the #define/include-once pattern
3. Adds support for including through the stb/ prefix path while maintaining compatibility with the stb_ headers. stb/image.h and stb_image.h are both valid include paths for stb_image.h
4. Automatically downloads the latest version of stb

#### Use
The preferred way to download stb_cmake is with fetchcontent.

```cmake
include(FetchContent)
FetchContent_Declare(
    stb
    GIT_REPOSITORY https://gitlab.com/Wahlstrand/stb_cmake.git
    GIT_TAG        master
)
FetchContent_MakeAvailable(stb)
```
Then use stb as you would use any cmake target:
```cmake
add_executable(your_executable ...)
target_link_libraries(your_library stb)

add_library(your_library ...)
target_link_libraries(your_executable stb)
```
If you only want certain stb libraries, e.g. image, use the corresponding alias:
```cmake
add_executable(your_executable ...)
target_link_libraries(your_library stb::image)

add_library(your_library ...)
target_link_libraries(your_executable stb::image)
```
#### Scope
Currently only the image library is supported. Increasing support for other libraries is easy. Pull requests are most welcome.

#### Requirements
Requires cmake 3.14 or later
